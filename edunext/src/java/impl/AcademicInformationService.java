package impl;

import dal.DBContext;
import entity.Classroom;
import entity.Course;
import entity.Question;
import entity.Slot;
import service.IAcademicInformationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AcademicInformationService extends DBContext implements IAcademicInformationService {

    Connection conn;

    public AcademicInformationService() {
        try {
            this.conn = getConnection();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Course> getAllCourses() {
        List<Course> courses = new ArrayList<>();
        String sql = "SELECT * FROM Course";

        try (
                PreparedStatement ps = conn.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Course course = new Course();
                course.setCourseID(rs.getInt("courseID"));
                course.setCourseName(rs.getString("courseName"));
                course.setCreatedDate(rs.getDate("createdDate"));
                course.setUpdatedDate(rs.getDate("updatedDate"));
                course.setDeleted(rs.getBoolean("isDeleted"));
                courses.add(course);
            }

        } catch (Exception e) {
            System.out.println("Error fetching courses: " + e.getMessage());
        }

        return courses;
    }

    @Override
    public List<Classroom> getAllClassrooms() {
        List<Classroom> classrooms = new ArrayList<>();
        String sql = "SELECT * FROM Classroom";

        try (
                PreparedStatement ps = conn.prepareStatement(sql); ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Classroom classroom = new Classroom();
                classroom.setClassID(rs.getInt("classID"));
                classroom.setClassName(rs.getString("className"));
                classroom.setCreatedDate(rs.getDate("createdDate"));
                classroom.setUpdatedDate(rs.getDate("updatedDate"));
                classroom.setDeleted(rs.getBoolean("isDeleted"));
                classroom.setSemester(rs.getString("semester"));
                classrooms.add(classroom);
            }

        } catch (Exception e) {
            System.out.println("Error fetching classrooms: " + e.getMessage());
        }

        return classrooms;
    }

    public Classroom getClassroomById(int classID) {
        String sql = "SELECT * FROM Classroom WHERE classID = ?";
        Classroom classroom = null;

        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, classID);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    classroom = new Classroom();
                    classroom.setClassID(rs.getInt("classID"));
                    classroom.setClassName(rs.getString("className"));
                    classroom.setCreatedDate(rs.getDate("createdDate"));
                    classroom.setUpdatedDate(rs.getDate("updatedDate"));
                    classroom.setDeleted(rs.getBoolean("isDeleted"));
                    classroom.setSemester(rs.getString("semester"));
                }
            }
        } catch (Exception e) {
            System.out.println("Error fetching classroom: " + e.getMessage());
        }
        return classroom;
    }

    @Override
    public List<Course> getCoursesByClassroomId(int classId) {
        List<Course> courses = new ArrayList<>();
        String sql = "SELECT c.* FROM Course c "
                + "JOIN CourseInClass cic ON c.courseID = cic.courseID "
                + "WHERE cic.classID = ?";

        try (Connection conn = getConnection(); PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, classId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Course course = new Course();
                course.setCourseID(rs.getInt("courseID"));
                course.setCourseName(rs.getString("courseName"));
                course.setCreatedDate(rs.getDate("createdDate"));
                course.setUpdatedDate(rs.getDate("updatedDate"));
                course.setDeleted(rs.getBoolean("isDeleted"));
                courses.add(course);
            }

        } catch (Exception e) {
            System.out.println("Error fetching courses by class ID: " + e.getMessage());
        }

        return courses;
    }

    @Override
    public Course getCourseById(int courseId) {
        String sql = "SELECT * FROM Course WHERE courseID = ? AND isDeleted = 0";
        Course course = null;

        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, courseId);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    course = new Course();
                    course.setCourseID(rs.getInt("courseID"));
                    course.setCourseName(rs.getString("courseName"));
                    course.setCreatedDate(rs.getDate("createdDate"));
                    course.setUpdatedDate(rs.getDate("updatedDate"));
                    course.setDeleted(rs.getBoolean("isDeleted"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Handle the exception
        }

        return course;
    }

    @Override
    public List<Slot> getSlotsByCourseId(int courseId) {
        List<Slot> slots = new ArrayList<>();
        String sql = "SELECT * FROM Slot WHERE courseID = ?";

        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, courseId);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Slot slot = new Slot();
                    slot.setSlotID(rs.getInt("slotID"));
                    slot.setCourseID(rs.getInt("courseID"));
                    slot.setSlotName(rs.getString("slotName"));
                    slot.setCreatedDate(rs.getDate("createdDate"));
                    slot.setUpdatedDate(rs.getDate("updatedDate"));
                    slot.setDeleted(rs.getBoolean("isDeleted"));
                    slot.setMessage(rs.getString("message"));
                    slots.add(slot);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Handle the exception
        }
        return slots;
    }

    @Override
    public List<Question> getQuestionsBySlotId(int slotId) {
        List<Question> questions = new ArrayList<>();
        String sql = "SELECT * FROM Question WHERE slotID = ?";

        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, slotId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Question question = new Question();
                question.setQuestionID(rs.getInt("questionID"));
                question.setSlotID(rs.getInt("slotID"));
                question.setQuestionTitle(rs.getString("questionTitle"));
                question.setQuestionContent(rs.getString("questionContent"));
                question.setCreatedDate(rs.getDate("createdDate"));
                question.setDeleted(rs.getBoolean("isDeleted"));
                questions.add(question);
            }

        } catch (Exception e) {
            System.out.println("Error fetching questions by slot ID: " + e.getMessage());
        }
        return questions;
    }
    
    @Override
    public Slot getSlotById(int slotId) {
        String sql = "SELECT * FROM Slot WHERE slotID = ?";
        Slot slot = null;
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, slotId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    slot = new Slot();
                    slot.setSlotID(rs.getInt("slotID"));
                    slot.setCourseID(rs.getInt("courseID")); // Adjust based on your schema
                    slot.setSlotName(rs.getString("slotName"));
                    slot.setCreatedDate(rs.getDate("createdDate"));
                    slot.setUpdatedDate(rs.getDate("updatedDate"));
                    slot.setDeleted(rs.getBoolean("isDeleted"));
                    slot.setMessage(rs.getString("message"));
                }
            }
        } catch (Exception e) {
            System.out.println("Error fetching slot by ID: " + e.getMessage());
        }
        return slot;
    }
    
    @Override
    public void updateSlotMessage(int slotID, String newMessage) {
        String sqlCheck = "SELECT message, updatedDate FROM Slot WHERE slotID = ?";
        String sqlUpdate = "UPDATE Slot SET message = ?, updatedDate = ? WHERE slotID = ?";
        String sqlUpdateNewMessage = "UPDATE Slot SET message = ?, updatedDate = ? WHERE slotID = ?";

        try (PreparedStatement psCheck = conn.prepareStatement(sqlCheck)) {
            psCheck.setInt(1, slotID);
            try (ResultSet rs = psCheck.executeQuery()) {
                if (rs.next()) {
                    String currentMessage = rs.getString("message");
                    Date currentUpdatedDate = rs.getDate("updatedDate");

                    try (PreparedStatement psUpdate = conn.prepareStatement(sqlUpdate)) {
                        if (currentMessage == null || currentUpdatedDate == null) {
                            psUpdate.setString(1, newMessage);
                            psUpdate.setDate(2, new java.sql.Date(new Date().getTime()));
                            psUpdate.setInt(3, slotID);
                        } else {
                            psUpdate.setString(1, newMessage);
                            psUpdate.setDate(2, new java.sql.Date(new Date().getTime()));
                            psUpdate.setInt(3, slotID);
                        }
                        psUpdate.executeUpdate();
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error updating slot message: " + e.getMessage());
        }
    }
    
    @Override
    public void addOrUpdateQuestion(int slotID, String questionContent) {
        String sqlCheck = "SELECT * FROM Question WHERE slotID = ?";
        String sqlInsert = "INSERT INTO Question (slotID, questionTitle, questionContent, createdDate, isDeleted) VALUES (?, ?, ?, ?, ?)";
        String sqlUpdate = "UPDATE Question SET questionContent = ?, createdDate = ? WHERE slotID = ?";

        try (PreparedStatement psCheck = conn.prepareStatement(sqlCheck)) {
            psCheck.setInt(1, slotID);
            ResultSet rs = psCheck.executeQuery();

            if (rs.next()) {
                try (PreparedStatement psUpdate = conn.prepareStatement(sqlUpdate)) {
                    psUpdate.setString(1, questionContent);
                    psUpdate.setDate(2, new java.sql.Date(new Date().getTime()));
                    psUpdate.setInt(3, slotID);
                    psUpdate.executeUpdate();
                }
            } else {
                try (PreparedStatement psInsert = conn.prepareStatement(sqlInsert)) {
                    psInsert.setInt(1, slotID);
                    psInsert.setString(2, "Default Title"); // You may replace this with actual title if available
                    psInsert.setString(3, questionContent);
                    psInsert.setDate(4, new java.sql.Date(new Date().getTime()));
                    psInsert.setBoolean(5, false); // Assuming `isDeleted` is false for new questions
                    psInsert.executeUpdate();
                }
            }
        } catch (Exception e) {
            System.out.println("Error adding or updating question: " + e.getMessage());
        }
    }
    
    

}
