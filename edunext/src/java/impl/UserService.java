/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package impl;

import dal.DBContext;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import service.IUserService;
/**
 *
 * @author Legion
 */
public class UserService extends DBContext implements IUserService{
    
    Connection connection;

    public UserService() {
        try {
            this.connection = getConnection();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    @Override
    public User authenticate(String username, String password) {
        String query = "SELECT * FROM [User] WHERE Username = ? AND Password = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                User user = new User();
                user.setUserID(rs.getInt("UserId"));
                user.setName(rs.getString("Name"));
                user.setEmail(rs.getString("Email"));

                return user;
            }
        } catch (SQLException e) {
            System.out.println("authenticate: " + e.getMessage());
        }
        return null;
    }
    
    
}
