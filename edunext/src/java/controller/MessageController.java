/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import impl.AcademicInformationService;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author chun
 */
public class MessageController extends HttpServlet {
    private AcademicInformationService academicService;

    @Override
    public void init() throws ServletException {
        academicService = new AcademicInformationService();
    }
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MessageControler</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MessageControler at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    public int extractSlotNumber(String slotString) {
        if (slotString != null && slotString.startsWith("Slot ")) {
            try {
                return Integer.parseInt(slotString.substring(5).trim());
            } catch (NumberFormatException e) {
                // Handle the case where the substring is not a valid integer
                e.printStackTrace();
            }
        }
        // Return a default value or throw an exception if the input is not valid
        return -1;
    }


    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String check = request.getParameter("check");
        int slotID = extractSlotNumber(request.getParameter("slotID"));
        String questionContent = request.getParameter("textarea");
        System.out.println(check + "123");
        System.out.println("ok");
        academicService.updateSlotMessage(slotID, questionContent);
        response.sendRedirect("course-detail?courseID=" + request.getParameter("courseID") + "&slotID=" + slotID + "&check=" + "check");

    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
