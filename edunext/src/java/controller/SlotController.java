package controller;

import entity.Course;
import entity.Question;
import entity.Slot;
import impl.AcademicInformationService;
import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class SlotController extends HttpServlet {
    private AcademicInformationService academicService;

    @Override
    public void init() throws ServletException {
        academicService = new AcademicInformationService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve parameters from the request
        String slotIdStr = request.getParameter("slotID");
        String courseIdStr = request.getParameter("courseID");

        // Convert parameters to integers
        int slotId = Integer.parseInt(slotIdStr);
        int courseId = Integer.parseInt(courseIdStr);
        System.out.println("slot" + slotId);
        System.out.println("course" + courseId);

        // Fetch data
        Course course = academicService.getCourseById(courseId);
        List<Question> questions = academicService.getQuestionsBySlotId(slotId);
        List<Slot> slots = academicService.getSlotsByCourseId(courseId);

        // Set attributes and forward to the JSP
        request.setAttribute("course", course);
        request.setAttribute("slots", slots);
        request.setAttribute("courseId", courseId);
        request.setAttribute("questions", questions);

        // Forward to a specific JSP or part of the page
        request.getRequestDispatcher("/course-detail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Implementation for POST request (if needed)
    }

    @Override
    public String getServletInfo() {
        return "Slot Controller";
    }
}
