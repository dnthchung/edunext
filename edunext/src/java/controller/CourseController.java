package controller;

import entity.Course;
import entity.Question;
import entity.Slot;
import impl.AcademicInformationService;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(name = "CourseController", urlPatterns = {"/course-detail"})
public class CourseController extends HttpServlet {
    
    private AcademicInformationService academicService;
    
    @Override
    public void init() throws ServletException {
        academicService = new AcademicInformationService();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CourseController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CourseController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int courseId = Integer.parseInt(request.getParameter("courseID"));
        System.out.println("get");
        String slotIdStr = request.getParameter("slotID");
        String check = request.getParameter("check");
//        System.out.println("check"  + check);
        int slotId = 1; // Giá trị mặc định là 1 nếu slotIdStr là null hoặc rỗng
        if (slotIdStr != null && !slotIdStr.isEmpty() && check.equals("check")) {
            slotId = Integer.parseInt(slotIdStr);
//            System.out.println("haha1 -> " + slotId);

//            System.out.println("hi" + slotId);
            Slot slotDetail = academicService.getSlotById(slotId);
            request.setAttribute("slotID", "Slot " + slotId);
            request.setAttribute("slotDetail", slotDetail);
            Course course = academicService.getCourseById(courseId);
            List<Slot> slots = academicService.getSlotsByCourseId(courseId);
            List<Question> questions = academicService.getQuestionsBySlotId(slotId);

            request.setAttribute("course", course);
            request.setAttribute("slots", slots);
            
//            System.out.println(slots);
            
            request.setAttribute("courseId", courseId);
            request.setAttribute("questions", questions);
            request.getRequestDispatcher("/course-detail.jsp").forward(request, response);
//            
                return;
        }

        
        Course course = academicService.getCourseById(courseId);
        List<Slot> slots = academicService.getSlotsByCourseId(courseId);
        List<Question> questions = academicService.getQuestionsBySlotId(1);
        System.out.println("==slotId" + slotId);
//        System.out.println("haha2 -> " + slotId);
        request.setAttribute("slotIdDef", slotId);
        request.setAttribute("course", course);
        request.setAttribute("slots", slots);
        System.out.println(slots);
        request.setAttribute("courseId", courseId);
        request.setAttribute("questions", questions);

        
        request.getRequestDispatcher("/course-detail.jsp").forward(request, response);
    }

    public int extractSlotNumber(String slotString) {
        if (slotString != null && slotString.startsWith("Slot ")) {
            try {
                return Integer.parseInt(slotString.substring(5).trim());
            } catch (NumberFormatException e) {
                // Handle the case where the substring is not a valid integer
                e.printStackTrace();
            }
        }
        // Return a default value or throw an exception if the input is not valid
        return -1;
    }


    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String check = request.getParameter("check");
        int slotID = extractSlotNumber(request.getParameter("slotID"));
        String questionContent = request.getParameter("textarea");
        System.out.println(check + "123");

        if (check != null) {
            if (check.equalsIgnoreCase("edit")) {
                System.out.println("edit");
                academicService.addOrUpdateQuestion(slotID, questionContent);
            } else if (check.equalsIgnoreCase("add")) {
                System.out.println("add");
//                academicService.updateSlotMessage(slotID, questionContent);
            }
        }

        // Redirect back to the course detail page
        response.sendRedirect("course-detail?courseID=" + request.getParameter("courseID") + "&slotID=" + slotID+ "&check=" + "check");
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
