/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package service;

import entity.Classroom;
import entity.Course;
import entity.Question;
import entity.Slot;
import java.util.List;

/**
 *
 * @author khoa2
 */
public interface IAcademicInformationService {

    List<Course> getAllCourses();

    List<Classroom> getAllClassrooms();

    Classroom getClassroomById(int classID);

    List<Course> getCoursesByClassroomId(int classId);

    Course getCourseById(int courseId);

    List<Slot> getSlotsByCourseId(int courseId);

    List<Question> getQuestionsBySlotId(int slotId);
    
    Slot getSlotById(int slotId);
    
    void updateSlotMessage(int slotID, String newMessage);

    void addOrUpdateQuestion(int slotID, String questionContent);
}
