/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package service;

import entity.User;

/**
 *
 * @author chun
 */
public interface IUserService {
    public User authenticate(String username, String password);
}
