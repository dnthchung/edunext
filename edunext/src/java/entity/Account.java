package entity;

public class Account {
    private int accountID;
    private String username;
    private String password;
    private int userID;

    public Account() {
    }

    public Account(int accountID, String username, String password, int userID) {
        this.accountID = accountID;
        this.username = username;
        this.password = password;
        this.userID = userID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    
    
}
