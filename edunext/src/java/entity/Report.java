package entity;

import java.util.Date;

public class Report {
    private int reportID;
    private String reportTitle;
    private String reportDetail;
    private Date createdDate;
    private boolean isSolved;
    private boolean isRead;
    private Date solvedDate;
    private int createdBy;

    public Report() {
    }

    public Report(int reportID, String reportTitle, String reportDetail, Date createdDate, boolean isSolved, boolean isRead, Date solvedDate, int createdBy) {
        this.reportID = reportID;
        this.reportTitle = reportTitle;
        this.reportDetail = reportDetail;
        this.createdDate = createdDate;
        this.isSolved = isSolved;
        this.isRead = isRead;
        this.solvedDate = solvedDate;
        this.createdBy = createdBy;
    }

    public int getReportID() {
        return reportID;
    }

    public void setReportID(int reportID) {
        this.reportID = reportID;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getReportDetail() {
        return reportDetail;
    }

    public void setReportDetail(String reportDetail) {
        this.reportDetail = reportDetail;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isSolved() {
        return isSolved;
    }

    public void setSolved(boolean isSolved) {
        this.isSolved = isSolved;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public Date getSolvedDate() {
        return solvedDate;
    }

    public void setSolvedDate(Date solvedDate) {
        this.solvedDate = solvedDate;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }
}
