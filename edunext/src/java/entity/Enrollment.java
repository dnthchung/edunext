package entity;

public class Enrollment {
    private int userID;
    private int classID;

    public Enrollment() {
    }

    public Enrollment(int userID, int classID) {
        this.userID = userID;
        this.classID = classID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }
}
