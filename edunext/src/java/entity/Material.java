package entity;

import java.util.Date;

public class Material {
    private int materialID;
    private int slotID;
    private String materialName;
    private Date createdDate;
    private boolean isDeleted;

    public Material() {
    }

    public Material(int materialID, int slotID, String materialName, Date createdDate, boolean isDeleted) {
        this.materialID = materialID;
        this.slotID = slotID;
        this.materialName = materialName;
        this.createdDate = createdDate;
        this.isDeleted = isDeleted;
    }

    public int getMaterialID() {
        return materialID;
    }

    public void setMaterialID(int materialID) {
        this.materialID = materialID;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
