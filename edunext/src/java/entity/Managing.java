package entity;

public class Managing {
    private int userID;
    private int classID;

    public Managing() {
    }

    public Managing(int userID, int classID) {
        this.userID = userID;
        this.classID = classID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }
}
