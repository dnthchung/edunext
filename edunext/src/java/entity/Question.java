package entity;

import java.util.Date;

public class Question {
        private int questionID;
        private int slotID;
        private String questionTitle;
        private String questionContent;
        private Date createdDate;
        private boolean isDeleted;

    public Question() {
    }

    public Question(int questionID, int slotID, String questionTitle, String questionContent, Date createdDate, boolean isDeleted) {
        this.questionID = questionID;
        this.slotID = slotID;
        this.questionTitle = questionTitle;
        this.questionContent = questionContent;
        this.createdDate = createdDate;
        this.isDeleted = isDeleted;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return super.toString(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
}
