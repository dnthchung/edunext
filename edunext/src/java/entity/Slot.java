package entity;

import java.util.Date;

public class Slot {
    private int slotID;
    private int courseID;
    private String slotName;
    private Date createdDate;
    private Date updatedDate;
    private String message;
    private boolean isDeleted;

    public Slot() {
    }

    public Slot(int slotID, int courseID, String slotName, Date createdDate, Date updatedDate, String message, boolean isDeleted) {
        this.slotID = slotID;
        this.courseID = courseID;
        this.slotName = slotName;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.message = message;
        this.isDeleted = isDeleted;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return super.toString(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    
}
