package entity;

import java.util.Date;

public class Answer {
    private int answerID;
    private int questionID;
    private String answerContent;
    private Date createdDate;
    private boolean isDeleted;

    public Answer() {
    }

    public Answer(int answerID, int questionID, String answerContent, Date createdDate, boolean isDeleted) {
        this.answerID = answerID;
        this.questionID = questionID;
        this.answerContent = answerContent;
        this.createdDate = createdDate;
        this.isDeleted = isDeleted;
    }

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
