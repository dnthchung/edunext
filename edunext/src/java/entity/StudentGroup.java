package entity;

import java.util.Date;

public class StudentGroup {
    private int studentGroupID;
    private int slotID;
    private String studentGroupName;
    private int numberOfStudents;
    private Date createdDate;

    public StudentGroup() {
    }

    public StudentGroup(int studentGroupID, int slotID, String studentGroupName, int numberOfStudents, Date createdDate) {
        this.studentGroupID = studentGroupID;
        this.slotID = slotID;
        this.studentGroupName = studentGroupName;
        this.numberOfStudents = numberOfStudents;
        this.createdDate = createdDate;
    }

    public int getStudentGroupID() {
        return studentGroupID;
    }

    public void setStudentGroupID(int studentGroupID) {
        this.studentGroupID = studentGroupID;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public String getStudentGroupName() {
        return studentGroupName;
    }

    public void setStudentGroupName(String studentGroupName) {
        this.studentGroupName = studentGroupName;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
