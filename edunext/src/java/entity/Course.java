package entity;

import java.util.Date;

public class Course {
    private int courseID;
    private String courseName;
    private Date createdDate;
    private Date updatedDate;
    private boolean isDeleted;

    public Course() {
    }

    public Course(int courseID, String courseName, Date createdDate, Date updatedDate, boolean isDeleted) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.isDeleted = isDeleted;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return "Course{" + "courseID=" + courseID + ", courseName=" + courseName + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", isDeleted=" + isDeleted + '}';
    }
    
     
}
