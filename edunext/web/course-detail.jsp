<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Course Detail</title>
        <link rel="icon" type="image/x-icon" href="./Images/FPT_logo_2010.png">
        <link rel="stylesheet" href="./CSS/course-detail.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/easymde/dist/easymde.min.css">
    </head>
    <body>
        <div class="d-flex flex-row">
            <div class="d-flex flex-column flex-shrink-0 sidebar-wrap" id="sidebar">
                <a href="#" class="text-decoration-none logo-wrap">
                    <div class="icon-wrap">
                        <img src="./Images/FPT_logo_2010.png" class="mlogo" />
                    </div>
                    <span style="font-weight: bolder; font-size: 20px; color: #000;">EduNext</span>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto mt-5">
                    <li class="nav-item">
                        <a href="home" class="nav-link " aria-current="page">
                            <div class="icon-wrap">
                                <i data-lucide="home"></i>
                            </div>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="home" class="nav-link " aria-current="page">
                            <div class="icon-wrap">
                                <i data-lucide="school"></i>
                            </div>
                            <span>Course</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="home" class="nav-link " aria-current="page">
                            <div class="icon-wrap">
                                <i data-lucide="users"></i>
                            </div>
                            <span>Users</span>
                        </a>
                    </li>
                    <hr>
                </ul>
            </div>
            <div class="main-content">
                <nav class="navbar navbar-expand-sm">
                    <div class="container-fluid">
                        <div class="mpage-name">
                            <h2 style="color: black;"><i>EduNext</i></h2>
                        </div>
                        <div class="collapse navbar-collapse" id="mynavbar">
                            <ul class="navbar-nav me-auto"></ul>
                            <div class="d-flex user-area">
                                <div class="user-info d-flex flex-column align-items-center">
                                    <span class="username">Đoàn Thành Chung</span>
                                    <p>Admin</p>
                                </div>
                                <div style="padding: 15px;"><i data-lucide="user"></i></div>
                                <div class="d-flex align-items-center">
                                    <a href="#" onclick="confirmLogout()" style="font-style: italic; color: black; margin-right: 20px;">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="container-fluid mt-4">
                    <c:forEach var="slot" items="${slots}">
                        <div class="row mb-4">
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-text my-card">
                                        <b style="color: #0e6efd;">Filter status</b>
                                    </div>
                                    <select class="form-select my-card" id="prepend-text-single-field">
                                        <option>All Activities</option>
                                        <option>Hidden</option>
                                        <option>On Going</option>
                                        <option>Cancelled</option>
                                        <option>Not Started</option>
                                        <option>Holistic</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <form id="slotForm" action="course-detail" method="get">
                                    <div class="input-group">
                                        <div class="input-group-text my-card">
                                            <b style="color: #0e6efd;">Slot</b>
                                        </div>
                                        <input type="hidden" name="courseID" value="${courseId}" />
                                        <input type="hidden" name="check" value="check" />
                                        <select class="form-select my-card" id="slotDropdown" name="slotID" onchange="document.getElementById('slotForm').submit();">
                                            <c:choose>
                                                <c:when test="${not empty slotDetail.slotName}">
                                                    <option value="${slotDetail.slotName}" selected disabled="true">${slotDetail.slotName}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${slotID}" selected disabled="true">${slotID}</option>
                                                </c:otherwise>
                                            </c:choose>
                                            <!-- Slot options -->
                                            <c:forEach var="i" begin="1" end="10">
                                                <option value="${i}">Slot ${i}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-1 col-md-4 col-sm-6"></div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <button id="bottone5">
                                    <i data-lucide="upload"></i> Upload materials
                                </button>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <button id="bottone5">
                                    <i data-lucide="users-round"></i> Manage students
                                </button>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-5">
                                <div class="mb-4" data-bs-toggle="collapse" data-bs-target="#slot-${slotDetail.slotID}" aria-expanded="false" aria-controls="slot-${slot.slotID}">
                                    <div class="card my-card">
                                        <div class="card-body">
                                            <h5 class="card-title">${slotDetail.getSlotName()} <c:if test="${slotDetail.getSlotName() == null}"> Slot ${slotIdDef} </c:if> </h5>
                                                <p class="card-text">Slot Message: 
                                                <c:if test="${empty slotDetail.message}">
                                                    <span style="color: red; font-style: italic;">No message for this slot</span>
                                                </c:if>
                                                <c:if test="${not empty slotDetail.message}">
                                                    <i>${slotDetail.message}</i>
                                                </c:if>
                                            </p>
                                            <p class="card-text d-flex justify-content-end">
                                                <i style="margin-top: 3px; height: 18px; stroke: black;" data-lucide="calendar-days"></i>
                                                <span>12:50 07/05/2024 - 15:10 07/05/2024</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="collapse" id="slot-${slotDetail.slotID}">
                                    <div class="card card-body">
                                        <div class="card-body">
                                            <c:forEach items="${questions}" var="question">
                                                <div class="question-row mb-4" style="cursor: pointer;">
                                                    <div class="question-row1">
                                                        <div class="question-icon">
                                                            <i data-lucide="circle-help"></i>
                                                        </div>
                                                        <div class="flex-grow-1">
                                                            <b class="mb-0">Q${question.questionID}</b>
                                                        </div>
                                                    </div>
                                                    <div class="status-ongoing-container">
                                                        <div class="status-ongoing">On-going</div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-6">
                                <div class="card my-card">
                                    <div class="card-body">
                                        <c:forEach items="${questions}" var="question">
                                            <div class="d-flex justify-content-center">
                                                <h4>${slotDetail.slotName} <c:if test="${slotDetail.getSlotName() == null}"> Slot ${slotIdDef} </c:if> </h4>
                                                </div>
                                                <h5 class="card-title">Question:</h5>
                                                <p>${question.questionContent}</p>
                                        </c:forEach>
                                        <div class="d-flex justify-content-end mb-2">
                                            <button class="btn btn-outline-secondary" data-bs-toggle="collapse" data-bs-target="#optionButtons" aria-expanded="false" aria-controls="optionButtons">Option</button>
                                        </div>
                                        <div class="collapse" id="optionButtons">
                                            <div class="d-flex justify-content-end">
                                                <button class="btn btn-outline-primary me-2" data-bs-toggle="modal" data-bs-target="#myEditModal">Edit</button>
                                                <button class="btn btn-outline-danger">Delete</button>
                                            </div>
                                        </div>
                                        <form id="messageForm" action="course-detail" method="post"> 
                                            <div class="modal fade" id="myEditModal" tabindex="-1" aria-labelledby="myEditModalLabel" aria-hidden="true"> 
                                                <div class="modal-dialog"> 
                                                    <div class="modal-content"> 
                                                        <div class="modal-header"> 
                                                            <h5 class="modal-title" id="myEditModalLabel">Edit Question</h5> 
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> 
                                                        </div> 
                                                        <div class="modal-body"> 
                                                            <textarea name="textarea" id="messageInput" class="form-control" rows="5" placeholder="enter question here..."></textarea> 
                                                        </div> 
                                                        <input type="hidden" id="courseID" name="courseID" value="${courseId}" /> 
                                                        <input type="hidden" id="slotID" name="slotID" value="${slotID}" /> 
                                                        <input type="hidden" id="check" name="check" value="edit" /> 
                                                        <div class="modal-footer"> 
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> 
                                                            <button type="submit" class="btn btn-primary " onclick="confirmAddMessage2(event)">Save changes</button> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </form> 
                                        <hr>
                                        <div class="d-flex justify-content-evenly" id="pills-tab" role="tablist">
                                            <button class="btn btn-outline-primary active m-4" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-group" type="button" role="tab" aria-selected="true">Group</button>
                                            <button class="btn btn-outline-primary m-4" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-discuss" type="button" role="tab" aria-selected="false">Discuss</button>
                                            <button class="btn btn-outline-primary m-4" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-message" type="button" role="tab" aria-selected="false">Teacher's message</button>
                                        </div>
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="pills-group" role="tabpanel" aria-labelledby="pills-home-tab">
                                                <div class="row mt-4">
                                                    <div class="col-1"></div>
                                                    <div class="col-10">
                                                        <div class="my-collapse1" data-bs-toggle="collapse" data-bs-target="#collapseExampleGroup" aria-expanded="false" aria-controls="collapseExampleGroup">
                                                            <div class="container mt-4">
                                                                <div class="my-group-card">
                                                                    <span class="my-group-info">Group 1</span>
                                                                    <span class="my-group-students">- (8 students)</span>
                                                                    <span class="my-group-note">- Your Group</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="collapse mt-4" id="collapseExampleGroup">
                                                            <div class="card card-body">
                                                                <div class="container mb-4">
                                                                    <div class="my-profile-card">
                                                                        <div class="my-profile-info">
                                                                            <div class="my-profile-pic">
                                                                                <img src="https://via.placeholder.com/40" alt="Profile Picture" class="rounded-circle">
                                                                            </div>
                                                                            <div class="my-profile-details">
                                                                                <p class="my-profile-name">HE176077</p>
                                                                                <p class="my-profile-name">Đoàn Thành Chung</p>
                                                                                <p class="my-profile-email">chungdthe176077@fpt.edu.vn</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1"></div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="pills-discuss" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                <div class="row mt-4">
                                                    <div class="col-1"></div>
                                                    <div class="col-10">
                                                        <div class="row mb-4">
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-6">
                                                                <select class="form-select" id="jumpSlot">
                                                                    <option>Outside group</option>
                                                                    <option>Inside group</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3"></div>
                                                        </div>
                                                        <div class="container">
                                                            <div class="pagination-container">
                                                                <a href="#" class="pagination-button">First</a>
                                                                <a href="#" class="pagination-button">Prev</a>
                                                                <a href="#" class="pagination-button active">1</a>
                                                                <a href="#" class="pagination-button">2</a>
                                                                <a href="#" class="pagination-button">Next</a>
                                                                <a href="#" class="pagination-button">Last</a>
                                                            </div>
                                                        </div>
                                                        <div class="discussion-item mt-4">
                                                            <div class="discussion-avatar">
                                                                <img src="https://via.placeholder.com/50" alt="Avatar">
                                                            </div>
                                                            <div class="discussion-content">
                                                                <div class="discussion-header">Lưu Danh Lương (Group 3)</div>
                                                                <div class="discussion-timestamp">09-05-2024 08:05:61</div>
                                                                <div class="discussion-body"> NodeJS là một môi trường runtime đa nền tảng... </div>
                                                            </div>
                                                            <div class="ms-3 mt-2">
                                                                <button class="btn btn-outline-secondary btn-sm">
                                                                    <i data-lucide="star"></i> 0
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="discussion-item">
                                                            <div class="discussion-avatar">
                                                                <img src="https://via.placeholder.com/50" alt="Avatar">
                                                            </div>
                                                            <div class="discussion-content">
                                                                <div class="discussion-header">Nguyễn Duy Khánh (Group 3)</div>
                                                                <div class="discussion-timestamp">08-05-2024 20:05:00</div>
                                                                <div class="discussion-body">hi</div>
                                                            </div>
                                                            <div class="ms-3 mt-2">
                                                                <button class="btn btn-outline-secondary btn-sm">
                                                                    <i data-lucide="star"></i> 4
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1"></div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="pills-message" role="tabpanel" aria-labelledby="pills-contact-tab">
                                                <div class="row">
                                                    <div class="col-1"></div>
                                                    <form id="messageForm2" action="MessageController" method="post"> 
                                                        <input type="hidden" id="courseID" name="courseID" value="${courseId}" /> 
                                                        <input type="hidden" id="slotID" name="slotID" value="${slotID}" /> 
                                                        <input type="hidden" name="check" value="add" /> 
                                                        <div class="col-10"> 
                                                            <div class="mb-3"> 
                                                                <textarea class="form-control" id="exampleFormControlTextarea1" name="textarea" rows="3"></textarea> 
                                                                <button type="button" class="my-btn-send" onclick="confirmAddMessage(event)">ADD MESSAGE</button> 
                                                            </div> 
                                                            <div class="discussion-item mt-4"> 
                                                                <div class="discussion-avatar"> 
                                                                    <img src="https://via.placeholder.com/50" alt="Avatar"> 
                                                                </div> 
                                                                <div class="discussion-content"> 
                                                                    <div class="discussion-header">Đoàn Thành Chung</div> 
                                                                    <div class="discussion-timestamp"> 
                                                                        <c:if test="${not empty slotDetail.getUpdatedDate()}"> 
                                                                            ${slotDetail.getUpdatedDate()} 
                                                                        </c:if> 
                                                                    </div> 
                                                                    <div class="discussion-body"> 
                                                                        <c:if test="${empty slotDetail.message}"> 
                                                                            <span style="color: red; font-style: italic;">No message for this slot</span> 
                                                                        </c:if> 
                                                                        <c:if test="${not empty slotDetail.message}"> 
                                                                            ${slotDetail.message} 
                                                                        </c:if> 
                                                                    </div> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                    </form> 
                                                    <div class="col-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <button onclick="topFunction()" id="my-back-to-top" class="my-back-to-top">
            <i data-lucide="arrow-up"></i>
        </button>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/lucide@latest/dist/umd/lucide.js"></script>
        <script>lucide.createIcons();</script>
        <script>
            $('#small-select2-options-multiple-field').select2({
                theme: "bootstrap-5",
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                closeOnSelect: true,
                selectionCssClass: 'select2--small',
                dropdownCssClass: 'select2--small'
            });
        </script>
        <script src="https://cdn.jsdelivr.net/npm/easymde/dist/easymde.min.js"></script>
        <script>
            var easyMDE = new EasyMDE({
                element: document.getElementById('my-markdown-editor'),
                toolbar: ['bold', 'italic', 'strikethrough', 'heading', '|', 'quote', 'unordered-list', 'ordered-list', '|', 'link', 'image', 'code', '|', 'preview', 'side-by-side', 'fullscreen'],
                status: false,
                renderingConfig: {
                    codeSyntaxHighlighting: true
                }
            });
        </script>
        <script>
            var mybutton = document.getElementById("my-back-to-top");
            window.onscroll = function () {
                scrollFunction();
            };
            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    mybutton.style.display = "block";
                } else {
                    mybutton.style.display = "none";
                }
            }
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
        <script>
            function confirmAddMessage(event) {
                event.preventDefault(); // Prevent the default form submission
                const textarea = document.querySelector('#exampleFormControlTextarea1').value.trim();
                if (textarea === '') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Please enter content before submitting!',
                    });
                } else {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Do you want to add this message?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, add it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Show success toast notification
                            Swal.fire({
                                icon: 'success',
                                title: 'Message added successfully',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            // Submit form after the toast notification
                            setTimeout(() => {
                                document.forms["messageForm2"].submit();
                            }, 2000); // Delay for 2 seconds
                        }
                    });
                }
            }

            function confirmAddMessage2(event) {
                event.preventDefault(); // Prevent the default form submission
                const textarea = document.querySelector('#messageInput').value.trim();
                if (textarea === '') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Please enter content before submitting!',
                    });
                } else {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Do you want to save these changes?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, save it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Show success toast notification
                            Swal.fire({
                                icon: 'success',
                                title: 'Changes saved successfully',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            // Submit form after the toast notification
                            setTimeout(() => {
                                document.forms["messageForm"].submit();
                            }, 2000); // Delay for 2 seconds
                        }
                    });
                }
            }
        </script>
    </body>
</html>
