<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:forEach var="question" items="${questions}">
    <div class="question-row mb-4" style="cursor: pointer;" onclick="showQuestionDetails(${question.questionID})">
        <div class="question-row1">
            <div class="question-icon">
                <i data-lucide="circle-help"></i>
            </div>
            <div class="flex-grow-1">
                <b class="mb-0">${question.questionTitle}</b>
            </div>
        </div>
        <div class="status-ongoing-container">
            <div class="status-ongoing">On-going</div>
        </div>
    </div>
</c:forEach>
