<%-- 
    Document   : home = class view
    Created on : Jul 6, 2024, 8:36:56 AM
    Author     : chun
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="icon" type="image/x-icon" href="./Images/FPT_logo_2010.png">
        <!-- link to css  -->
        <link rel="stylesheet" href="./CSS/home.css">
        <!-- Bootstrap 5 CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

    </head>
    <body>
        <!-- side bar -->
        <div class="d-flex flex-row">
            <!-- side bar  -->
            <div class="d-flex flex-column flex-shrink-0 sidebar-wrap" id="sidebar">
                <a href="#" class="text-decoration-none logo-wrap">
                    <div class="icon-wrap">
                        <img src="./Images/FPT_logo_2010.png" class="mlogo" />
                    </div>
                    <span style="font-weight: bolder ;font-size: 20px; color: #000;">EduNext</span>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto mt-5">
                    <li class="nav-item">
                        <a href="home.jsp" class="nav-link " aria-current="page">
                            <div class="icon-wrap">
                                <i data-lucide="home"></i>
                            </div>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="home.jsp" class="nav-link " aria-current="page">
                            <div class="icon-wrap">
                                <i data-lucide="school"></i>
                            </div>
                            <span> Course</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="home.jsp" class="nav-link " aria-current="page">
                            <div class="icon-wrap">
                                <i data-lucide="users"></i>
                            </div>
                            <span>Users</span>
                        </a>
                    </li>
                    <hr>
                    </div>
                    <!-- navigation bar + main content-->
                    <div class="main-content">
                        <!-- navigation bar -->
                        <nav class="navbar navbar-expand-sm ">
                            <div class="container-fluid">
                                <div class="mpage-name">
                                    <h2 class="" style="color: black;">
                                        <i>EduNext</i>
                                    </h2>
                                </div>
                                <div class="collapse navbar-collapse" id="mynavbar">
                                    <ul class="navbar-nav me-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#"></a>
                                        </li>
                                    </ul>
                                    <!--đã login-->
                                    <div class="d-flex user-area">
                                        <div class="user-info d-flex flex-column align-items-center">
                                            <span class="username">User Name</span>
                                            <!-- <p>@chungdt3</p> -->
                                            <p>Admin</p>
                                        </div>
                                        <div style="padding: 15px;">
                                            <i style="height: 20px;" data-lucide="user"></i>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <a href="#" onclick="confirmLogout()"
                                               style="font-style: italic; color: black; margin-right: 20px;">Logout</a>
                                        </div>
                                    </div>
                                    <!--chưa login-->
                                    <!-- <div class="d-flex" style="gap: 25px;">
                                        <div class="button-2">
                                            <a href="login" style="text-decoration: none; color: #000;">Login</a>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </nav>
                        <!-- body -->
                        <div class="container-fluid mt-4">
                            <!-- content -->
                            <!-- A. Control  -->
                            <div class="container-fluid mt-4">
                                <!-- content -->
                                <!-- A. role student : show list courses -->
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-6 m-4">
                                        <!-- dropdown to choose semester -->
                                        <div class="input-group">
                                            <div class="input-group-text my-card">
                                                <b>Semester</b>
                                            </div>
                                            <select class="form-select my-card" id="prepend-text-single-field"
                                                    data-placeholder="Choose one thing">
                                                <option>Fall </option>
                                                <option>Summer</option>
                                                <option>Spring</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- search input for class name -->
                                    <div class="col-lg-3 col-md-4 col-sm-6 m-4">
                                        <div class="input-group">
                                            <input type="text" class="form-control my-card" placeholder="Search class name"
                                                   id="classSearch">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary " type="button"
                                                        style="border-top-left-radius: 0; border-bottom-left-radius: 0;"
                                                        onclick="searchClass()">Search</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- B. role teacher : show list class -->
                            <div class="row m-4">
                                <c:forEach var="classroom" items="${classrooms}">
                                    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                                        <div class="card my-card">
                                            <div class="card-body">
                                                <h5 class="card-title" style="font-size: 25px; margin-bottom: 15px">${classroom.className}</h5>
                                                <div style="border-top: 1px solid #e0e0e0;">
                                                    <p class="my-card-info" style="margin-top: 15px;">
                                                        <i data-lucide="book-text"></i>
                                                        <span class="cn">${classroom.semester}</span>
                                                        <br>
                                                        <i data-lucide="users-round"></i>
                                                        <span>Number of Students: <span class="nos">${classroom.numberOfStudents}</span></span>
                                                    </p>
                                                </div>
                                                <a href="/edunext/class-detail?classID=${classroom.classID}">
                                                    <button id="bottone5" style="padding: 5px 10px; font-size: 12px; line-height: 1;">
                                                        <i data-lucide="arrow-right-from-line"></i> Go to class
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- jQuery -->
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
            <!--Bootstrap 5-->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
                    integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
            crossorigin="anonymous"></script>
            <!--lucide-->
            <script src="https://unpkg.com/lucide@latest/dist/umd/lucide.js"></script>
            <script src="https://unpkg.com/lucide@latest"></script>
            <script>
                                                            lucide.createIcons();
            </script>
            <!-- skill multi choice -->
            <script>
                $('#small-select2-options-multiple-field').select2({
                    theme: "bootstrap-5",
                    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                    placeholder: $(this).data('placeholder'),
                    closeOnSelect: true,
                    selectionCssClass: 'select2--small',
                    dropdownCssClass: 'select2--small'
                });
            </script>
            <!-- search class function -->
            <script>
                function searchClass() {
                    const searchValue = document.getElementById('classSearch').value.toLowerCase();
                    const classCards = document.querySelectorAll('.card');

                    classCards.forEach(card => {
                        const className = card.querySelector('.card-title').textContent.toLowerCase();
                        if (className.includes(searchValue)) {
                            card.style.display = '';
                        } else {
                            card.style.display = 'none';
                        }
                    });
                }
            </script>
    </body>

</html>
