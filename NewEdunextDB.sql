CREATE DATABASE EdunextDB

USE EdunextDB
CREATE TABLE [User] (
  [userID] INT PRIMARY KEY IDENTITY(1, 1),
  [name] NVARCHAR(100) NOT NULL,
  [email] NVARCHAR(100) UNIQUE NOT NULL,
  [birthdate] DATE,
  [joinedDate] DATE NOT NULL,
  [roleID] INT NOT NULL
)
GO

CREATE TABLE [Account] (
  [acoountID] INT PRIMARY KEY IDENTITY(1, 1),
  [username] NVARCHAR(50),
  [password] NVARCHAR(50) NOT NULL,
  [userID] INT UNIQUE
)
GO

CREATE TABLE [Role] (
  [roleID] INT PRIMARY KEY IDENTITY(1, 1),
  [roleName] NVARCHAR(50) NOT NULL
)
GO

CREATE TABLE [Course] (
  [courseID] INT PRIMARY KEY IDENTITY(1, 1),
  [courseName] NVARCHAR(100) NOT NULL,
  [createdDate] DATE,
  [updatedDate] DATE,
  [isDeleted] BIT NOT NULL
)
GO

CREATE TABLE [Classroom] (
  [classID] INT PRIMARY KEY IDENTITY(1, 1),
  [className] NVARCHAR(100) NOT NULL,
  [createdDate] DATE,
  [updatedDate] DATE,
  [isDeleted] BIT NOT NULL,
  [semester] NVARCHAR(50) NOT NULL
)
GO

CREATE TABLE [Slot] (
  [courseID] INT,
  [slotID] INT PRIMARY KEY IDENTITY(1, 1),
  [slotName] NVARCHAR(100) NOT NULL,
  [createdDate] DATE,
  [updatedDate] DATE,
  [message] NVARCHAR(MAX),
  [isDeleted] BIT NOT NULL
)
GO

CREATE TABLE [Question] (
  [questionID] INT PRIMARY KEY IDENTITY(1, 1),
  [slotID] INT UNIQUE,
  [questionTitle] NVARCHAR(100) NOT NULL,
  [questionContent] NVARCHAR(MAX) NOT NULL,
  [createdDate] DATE,
  [isDeleted] BIT NOT NULL
)
GO

CREATE TABLE [Answer] (
  [answerID] INT PRIMARY KEY IDENTITY(1, 1),
  [questionID] INT UNIQUE,
  [answerContent] NVARCHAR(MAX) NOT NULL,
  [createdDate] DATE,
  [createdBy] INT,
  [isDeleted] BIT NOT NULL
)
GO

CREATE TABLE [StudentGroup] (
  [studentGroupID] INT PRIMARY KEY IDENTITY(1, 1),
  [slotID] INT,
  [studentGroupName] NVARCHAR(100) NOT NULL,
  [numberOfStudents] INT NOT NULL,
  [createdDate] DATE
)
GO

CREATE TABLE [Material] (
  [materialID] INT PRIMARY KEY IDENTITY(1, 1),
  [slotID] INT,
  [materialName] NVARCHAR(100) NOT NULL,
  [createdDate] DATE,
  [isDeleted] BIT NOT NULL
)
GO

CREATE TABLE [Report] (
  [reportID] INT PRIMARY KEY IDENTITY(1, 1),
  [reportTitle] NVARCHAR(100) NOT NULL,
  [reportDetail] NVARCHAR(MAX) NOT NULL,
  [createdDate] DATE,
  [createdBy] INT,
  [isSolved] BIT NOT NULL,
  [isRead] BIT NOT NULL,
  [solvedDate] DATE
)
GO

CREATE TABLE [UserInClass] (
  [userID] INT,
  [classID] INT,
  [role] NVARCHAR(50) NOT NULL,
  PRIMARY KEY ([classID], [userID])
)
GO

CREATE TABLE [CourseInClass] (
  [classID] INT,
  [courseID] INT,
  PRIMARY KEY ([courseID], [classID])
)
GO

ALTER TABLE [Account] ADD FOREIGN KEY ([userID]) REFERENCES [User] ([userID])
GO

ALTER TABLE [User] ADD FOREIGN KEY ([roleID]) REFERENCES [Role] ([roleID])
GO

ALTER TABLE [Slot] ADD FOREIGN KEY ([courseID]) REFERENCES [Course] ([courseID])
GO

ALTER TABLE [Question] ADD FOREIGN KEY ([slotID]) REFERENCES [Slot] ([slotID])
GO

ALTER TABLE [Answer] ADD FOREIGN KEY ([questionID]) REFERENCES [Question] ([questionID])
GO

ALTER TABLE [Answer] ADD FOREIGN KEY ([createdBy]) REFERENCES [User] ([userID])
GO

ALTER TABLE [StudentGroup] ADD FOREIGN KEY ([slotID]) REFERENCES [Slot] ([slotID])
GO

ALTER TABLE [Material] ADD FOREIGN KEY ([slotID]) REFERENCES [Slot] ([slotID])
GO

ALTER TABLE [Report] ADD FOREIGN KEY ([createdBy]) REFERENCES [User] ([userID])
GO

ALTER TABLE [UserInClass] ADD FOREIGN KEY ([classID]) REFERENCES [Classroom] ([classID])
GO

ALTER TABLE [UserInClass] ADD FOREIGN KEY ([userID]) REFERENCES [User] ([userID])
GO

ALTER TABLE [CourseInClass] ADD FOREIGN KEY ([courseID]) REFERENCES [Course] ([courseID])
GO

ALTER TABLE [CourseInClass] ADD FOREIGN KEY ([classID]) REFERENCES [Classroom] ([classID])
GO




-- Insert records into Role table
INSERT INTO [Role] (roleName) VALUES 
('Student'), 
('Manager'), 
('Lecturer');

-- Insert records into User table
INSERT INTO [User] (name, email, birthdate, joinedDate, roleID) VALUES 
('John Doe', 'johndoe@example.com', '1990-01-01', '2021-01-01', 1),
('Jane Smith', 'janesmith@example.com', '1985-02-02', '2021-02-01', 2),
('Alice Johnson', 'alicejohnson@example.com', '1992-03-03', '2021-03-01', 3),
('Bob Brown', 'bobbrown@example.com', '1988-04-04', '2021-04-01', 1),
('Charlie Davis', 'charliedavis@example.com', '1995-05-05', '2021-05-01', 2),
('David Wilson', 'davidwilson@example.com', '1983-06-06', '2021-06-01', 3),
('Eva Green', 'evagreen@example.com', '1994-07-07', '2021-07-01', 1),
('Frank White', 'frankwhite@example.com', '1987-08-08', '2021-08-01', 2),
('Grace Hall', 'gracehall@example.com', '1993-09-09', '2021-09-01', 3),
('Henry King', 'henryking@example.com', '1986-10-10', '2021-10-01', 1);

INSERT INTO [Account] (username, password, userID) VALUES 
('johndoe', '123', 1),
('janesmith', '123', 2),
('alicejohnson', '123', 3),
('bobbrown', '123', 4),
('charliedavis', '123', 5),
('davidwilson', '123', 6),
('evagreen', '123', 7),
('frankwhite', '123', 8),
('gracehall', '123', 9),
('henryking', '123', 10);

-- Insert records into Course table
INSERT INTO [Course] (courseName, createdDate, updatedDate, isDeleted) VALUES 
('Math 101', '2023-01-01', '2023-01-01', 0),
('Physics 201', '2023-02-01', '2023-02-01', 0),
('Chemistry 301', '2023-03-01', '2023-03-01', 0),
('Biology 101', '2023-04-01', '2023-04-01', 0),
('History 101', '2023-05-01', '2023-05-01', 0),
('Geography 201', '2023-06-01', '2023-06-01', 0),
('English 101', '2023-07-01', '2023-07-01', 0),
('Computer Science 101', '2023-08-01', '2023-08-01', 0),
('Art 101', '2023-09-01', '2023-09-01', 0),
('Music 101', '2023-10-01', '2023-10-01', 0);

-- Insert records into Classroom table
INSERT INTO [Classroom] (className, createdDate, updatedDate, isDeleted, semester) VALUES 
('SE1720', '2023-01-01', '2023-01-01', 0, 'Spring 2023'),
('SE1721', '2023-02-01', '2023-02-01', 0, 'Spring 2023'),
('SE1722', '2023-03-01', '2023-03-01', 0, 'Spring 2023'),
('SE1723', '2023-04-01', '2023-04-01', 0, 'Spring 2023'),
('SE1724', '2023-05-01', '2023-05-01', 0, 'Spring 2023'),
('SE1725', '2023-06-01', '2023-06-01', 0, 'Spring 2023'),
('SE1726', '2023-07-01', '2023-07-01', 0, 'Spring 2023'),
('SE1727', '2023-08-01', '2023-08-01', 0, 'Spring 2023'),
('SE1728', '2023-09-01', '2023-09-01', 0, 'Spring 2023'),
('SE1729', '2023-10-01', '2023-10-01', 0, 'Spring 2023');

-- Insert records into Slot table
INSERT INTO [Slot] (courseID, slotName, createdDate, updatedDate, message, isDeleted) VALUES 
(1, 'Slot 1', '2023-01-01', '2023-01-01', 'Welcome to Math 101', 0),
(2, 'Slot 2', '2023-02-01', '2023-02-01', 'Welcome to Physics 201', 0),
(3, 'Slot 3', '2023-03-01', '2023-03-01', 'Welcome to Chemistry 301', 0),
(4, 'Slot 4', '2023-04-01', '2023-04-01', 'Welcome to Biology 101', 0),
(5, 'Slot 5', '2023-05-01', '2023-05-01', 'Welcome to History 101', 0),
(6, 'Slot 6', '2023-06-01', '2023-06-01', 'Welcome to Geography 201', 0),
(7, 'Slot 7', '2023-07-01', '2023-07-01', 'Welcome to English 101', 0),
(8, 'Slot 8', '2023-08-01', '2023-08-01', 'Welcome to Computer Science 101', 0),
(9, 'Slot 9', '2023-09-01', '2023-09-01', 'Welcome to Art 101', 0),
(10, 'Slot 10', '2023-10-01', '2023-10-01', 'Welcome to Music 101', 0);

-- Insert records into Question table
INSERT INTO [Question] (slotID, questionTitle, questionContent, createdDate, isDeleted) VALUES 
(1, 'Question 1', 'What is 2+2?', '2023-01-01', 0),
(2, 'Question 2', 'What is the speed of light?', '2023-02-01', 0),
(3, 'Question 3', 'What is H2O?', '2023-03-01', 0),
(4, 'Question 4', 'What is DNA?', '2023-04-01', 0),
(5, 'Question 5', 'Who was the first President of the United States?', '2023-05-01', 0),
(6, 'Question 6', 'What is the capital of France?', '2023-06-01', 0),
(7, 'Question 7', 'What is the past tense of "run"?', '2023-07-01', 0),
(8, 'Question 8', 'What is a binary tree?', '2023-08-01', 0),
(9, 'Question 9', 'What are the primary colors?', '2023-09-01', 0),
(10, 'Question 10', 'What is a chord?', '2023-10-01', 0);

-- Insert records into Answer table
INSERT INTO [Answer] (questionID, answerContent, createdDate, createdBy, isDeleted) VALUES 
(1, '4', '2023-01-02', 1, 0),
(2, '299,792,458 m/s', '2023-02-02', 2, 0),
(3, 'Water', '2023-03-02', 3, 0),
(4, 'Deoxyribonucleic Acid', '2023-04-02', 4, 0),
(5, 'George Washington', '2023-05-02', 5, 0),
(6, 'Paris', '2023-06-02', 6, 0),
(7, 'Ran', '2023-07-02', 7, 0),
(8, 'A tree data structure in which each node has at most two children', '2023-08-02', 8, 0),
(9, 'Red, Blue, Yellow', '2023-09-02', 9, 0),
(10, 'A combination of three or more notes played simultaneously', '2023-10-02', 10, 0);

-- Insert records into StudentGroup table
INSERT INTO [StudentGroup] (slotID, studentGroupName, numberOfStudents, createdDate) VALUES 
(1, 'Group 1', 5, '2023-01-01'),
(2, 'Group 2', 6, '2023-02-01'),
(3, 'Group 3', 7, '2023-03-01'),
(4, 'Group 4', 8, '2023-04-01'),
(5, 'Group 5', 9, '2023-05-01'),
(6, 'Group 6', 10, '2023-06-01'),
(7, 'Group 7', 5, '2023-07-01'),
(8, 'Group 8', 6, '2023-08-01'),
(9, 'Group 9', 7, '2023-09-01'),
(10, 'Group 10', 8, '2023-10-01');

-- Insert records into Material table
INSERT INTO [Material] (slotID, materialName, createdDate, isDeleted) VALUES 
(1, 'Material 1', '2023-01-01', 0),
(2, 'Material 2', '2023-02-01', 0),
(3, 'Material 3', '2023-03-01', 0),
(4, 'Material 4', '2023-04-01', 0),
(5, 'Material 5', '2023-05-01', 0),
(6, 'Material 6', '2023-06-01', 0),
(7, 'Material 7', '2023-07-01', 0),
(8, 'Material 8', '2023-08-01', 0),
(9, 'Material 9', '2023-09-01', 0),
(10, 'Material 10', '2023-10-01', 0);

-- Insert records into Report table
INSERT INTO [Report] (reportTitle, reportDetail, createdDate, createdBy, isSolved, isRead, solvedDate) VALUES 
('Report 1', 'Detail of Report 1', '2023-01-01', 1, 1, 1, '2023-01-10'),
('Report 2', 'Detail of Report 2', '2023-02-01', 2, 0, 1, NULL),
('Report 3', 'Detail of Report 3', '2023-03-01', 3, 1, 0, '2023-03-10'),
('Report 4', 'Detail of Report 4', '2023-04-01', 4, 0, 1, NULL),
('Report 5', 'Detail of Report 5', '2023-05-01', 5, 1, 0, '2023-05-10'),
('Report 6', 'Detail of Report 6', '2023-06-01', 6, 0, 1, NULL),
('Report 7', 'Detail of Report 7', '2023-07-01', 7, 1, 0, '2023-07-10'),
('Report 8', 'Detail of Report 8', '2023-08-01', 8, 0, 1, NULL),
('Report 9', 'Detail of Report 9', '2023-09-01', 9, 1, 0, '2023-09-10'),
('Report 10', 'Detail of Report 10', '2023-10-01', 10, 0, 1, NULL);

-- Insert records into UserInClass table
INSERT INTO [UserInClass] (userID, classID, role) VALUES 
(1, 1, 'Student'),
(2, 2, 'Manager'),
(3, 3, 'Lecturer'),
(4, 4, 'Student'),
(5, 5, 'Manager'),
(6, 6, 'Lecturer'),
(7, 7, 'Student'),
(8, 8, 'Manager'),
(9, 9, 'Lecturer'),
(10, 10, 'Student');

-- Insert records into CourseInClass table
INSERT INTO [CourseInClass] (classID, courseID) VALUES 
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10);

