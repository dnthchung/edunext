-- Create Database
CREATE DATABASE EdunextSystemDB;
GO

-- Use the created database
USE EdunextSystemDB;
GO

-- Create Tables
CREATE TABLE [Roles] (
    [roleID] INT PRIMARY KEY,
    [roleName] NVARCHAR(50) NOT NULL
);

CREATE TABLE [Users] (
    [userID] INT PRIMARY KEY,
    [name] NVARCHAR(100) NOT NULL,
    [email] NVARCHAR(100) UNIQUE NOT NULL,
    [birthdate] DATE,
    [joinedDate] DATE NOT NULL,
    [roleID] INT,
    FOREIGN KEY ([roleID]) REFERENCES [Roles] ([roleID])
);

CREATE TABLE [Accounts] (
    [username] NVARCHAR(50) PRIMARY KEY,
    [password] NVARCHAR(50) NOT NULL,
    [userID] INT,
    FOREIGN KEY ([userID]) REFERENCES [Users] ([userID])
);

CREATE TABLE [Courses] (
    [courseID] INT PRIMARY KEY,
    [courseName] NVARCHAR(100) NOT NULL,
    [createdDate] DATE,
    [updatedDate] DATE,
    [isDeleted] BIT NOT NULL
);

CREATE TABLE [Classrooms] (
    [classID] INT PRIMARY KEY,
    [className] NVARCHAR(100) NOT NULL,
    [createdDate] DATE,
    [updatedDate] DATE,
    [isDeleted] BIT NOT NULL,
    [semester] NVARCHAR(50) NOT NULL
);

CREATE TABLE [Slots] (
    [slotID] INT PRIMARY KEY,
    [courseID] INT,
    [slotName] NVARCHAR(100) NOT NULL,
    [createdDate] DATE,
    [updatedDate] DATE,
    [message] NVARCHAR(MAX),
    [isDeleted] BIT NOT NULL,
    FOREIGN KEY ([courseID]) REFERENCES [Courses] ([courseID])
);

CREATE TABLE [Questions] (
    [questionID] INT PRIMARY KEY,
    [slotID] INT,
    [questionTitle] NVARCHAR(100) NOT NULL,
    [questionContent] NVARCHAR(MAX) NOT NULL,
    [createdDate] DATE,
    [isDeleted] BIT NOT NULL,
    FOREIGN KEY ([slotID]) REFERENCES [Slots] ([slotID])
);

CREATE TABLE [Answers] (
    [answerID] INT PRIMARY KEY,
    [questionID] INT,
    [answerContent] NVARCHAR(MAX) NOT NULL,
    [createdDate] DATE,
    [isDeleted] BIT NOT NULL,
    FOREIGN KEY ([questionID]) REFERENCES [Questions] ([questionID])
);

CREATE TABLE [StudentGroups] (
    [studentGroupID] INT PRIMARY KEY,
    [slotID] INT,
    [studentGroupName] NVARCHAR(100) NOT NULL,
    [numberOfStudents] INT NOT NULL,
    [createdDate] DATE,
    FOREIGN KEY ([slotID]) REFERENCES [Slots] ([slotID])
);

CREATE TABLE [Materials] (
    [materialID] INT PRIMARY KEY,
    [slotID] INT,
    [materialName] NVARCHAR(100) NOT NULL,
    [createdDate] DATE,
    [isDeleted] BIT NOT NULL,
    FOREIGN KEY ([slotID]) REFERENCES [Slots] ([slotID])
);

CREATE TABLE [Reports] (
    [reportID] INT PRIMARY KEY,
    [reportTitle] NVARCHAR(100) NOT NULL,
    [reportDetail] NVARCHAR(MAX) NOT NULL,
    [createdDate] DATE,
    [isSolved] BIT NOT NULL,
    [isRead] BIT NOT NULL,
    [solvedBy] INT,
    [solvedDate] DATE,
    [createdBy] INT,
    FOREIGN KEY ([createdBy]) REFERENCES [Users] ([userID]),
    FOREIGN KEY ([solvedBy]) REFERENCES [Users] ([userID])
);

CREATE TABLE [Enrollment] (
    [userID] INT,
    [classID] INT,
    PRIMARY KEY ([classID], [userID]),
    FOREIGN KEY ([classID]) REFERENCES [Classrooms] ([classID]),
    FOREIGN KEY ([userID]) REFERENCES [Users] ([userID])
);

CREATE TABLE [Teaching] (
    [userID] INT,
    [classID] INT,
    PRIMARY KEY ([classID], [userID]),
    FOREIGN KEY ([classID]) REFERENCES [Classrooms] ([classID]),
    FOREIGN KEY ([userID]) REFERENCES [Users] ([userID])
);

CREATE TABLE [Managing] (
    [userID] INT,
    [classID] INT,
    PRIMARY KEY ([classID], [userID]),
    FOREIGN KEY ([classID]) REFERENCES [Classrooms] ([classID]),
    FOREIGN KEY ([userID]) REFERENCES [Users] ([userID])
);

CREATE TABLE [CourseInClass] (
    [classID] INT,
    [courseID] INT,
    PRIMARY KEY ([courseID], [classID]),
    FOREIGN KEY ([courseID]) REFERENCES [Courses] ([courseID]),
    FOREIGN KEY ([classID]) REFERENCES [Classrooms] ([classID])
);


-- Sample data for Roles table
INSERT INTO [Roles] ([roleID], [roleName])
VALUES 
    (1, 'Admin'),
    (2, 'Teacher'),
    (3, 'Student');

-- Sample data for Users table
INSERT INTO [Users] ([userID], [name], [email], [birthdate], [joinedDate], [roleID])
VALUES 
    (1, 'John Doe', 'john.doe@example.com', '1990-01-01', '2020-01-01', 1),
    (2, 'Jane Smith', 'jane.smith@example.com', '1992-05-15', '2020-01-01', 2),
    (3, 'Michael Johnson', 'michael.johnson@example.com', '1985-09-30', '2020-01-01', 3);

-- Sample data for Accounts table
INSERT INTO [Accounts] ([username], [password], [userID])
VALUES 
    ('johndoe', 'password1', 1),
    ('janesmith', 'password2', 2),
    ('michaelj', 'password3', 3);

-- Sample data for Courses table
INSERT INTO [Courses] ([courseID], [courseName], [createdDate], [updatedDate], [isDeleted])
VALUES 
    (1, 'Mathematics', '2020-01-01', NULL, 0),
    (2, 'History', '2020-01-01', NULL, 0),
    (3, 'Physics', '2020-01-01', NULL, 0);

-- Sample data for Classrooms table
INSERT INTO [Classrooms] ([classID], [className], [createdDate], [updatedDate], [isDeleted], [semester])
VALUES 
    (1, 'Class A', '2020-01-01', NULL, 0, 'Spring'),
    (2, 'Class B', '2020-01-01', NULL, 0, 'Fall'),
    (3, 'Class C', '2020-01-01', NULL, 0, 'Summer');

-- Sample data for Slots table
INSERT INTO [Slots] ([slotID], [courseID], [slotName], [createdDate], [updatedDate], [message], [isDeleted])
VALUES 
    (1, 1, 'Math Lecture', '2020-01-01', NULL, 'Lecture on introductory mathematics', 0),
    (2, 2, 'History Seminar', '2020-01-01', NULL, 'Seminar on Ancient History', 0),
    (3, 3, 'Physics Lab', '2020-01-01', NULL, 'Laboratory session on physics experiments', 0);

-- Sample data for Questions table
INSERT INTO [Questions] ([questionID], [slotID], [questionTitle], [questionContent], [createdDate], [isDeleted])
VALUES 
    (1, 1, 'Math Question 1', 'What is calculus?', '2020-01-01', 0),
    (2, 2, 'History Question 1', 'Who founded Rome?', '2020-01-01', 0),
    (3, 3, 'Physics Question 1', 'What is Newton''s first law of motion?', '2020-01-01', 0);

-- Sample data for Answers table
INSERT INTO [Answers] ([answerID], [questionID], [answerContent], [createdDate], [isDeleted])
VALUES 
    (1, 1, 'Calculus is a branch of mathematics that deals with derivatives and integrals.', '2020-01-02', 0),
    (2, 2, 'Rome was founded by Romulus and Remus according to Roman mythology.', '2020-01-02', 0),
    (3, 3, 'Newton''s first law of motion states that an object will remain at rest or in uniform motion unless acted upon by an external force.', '2020-01-02', 0);

-- Sample data for StudentGroups table
INSERT INTO [StudentGroups] ([studentGroupID], [slotID], [studentGroupName], [numberOfStudents], [createdDate])
VALUES 
    (1, 1, 'Math Club', 20, '2020-01-01'),
    (2, 2, 'History Society', 15, '2020-01-01'),
    (3, 3, 'Physics Team', 25, '2020-01-01');

-- Sample data for Materials table
INSERT INTO [Materials] ([materialID], [slotID], [materialName], [createdDate], [isDeleted])
VALUES 
    (1, 1, 'Calculus Notes', '2020-01-01', 0),
    (2, 2, 'History Textbook', '2020-01-01', 0),
    (3, 3, 'Physics Lab Manual', '2020-01-01', 0);

-- Sample data for Reports table
INSERT INTO [Reports] ([reportID], [reportTitle], [reportDetail], [createdDate], [isSolved], [isRead], [solvedBy], [solvedDate], [createdBy])
VALUES 
    (1, 'Math Report', 'Report on recent math exam results.', '2020-01-01', 1, 1, 1, '2020-01-02', 1),
    (2, 'History Report', 'Report on historical research findings.', '2020-01-01', 1, 1, 2, '2020-01-02', 2),
    (3, 'Physics Report', 'Report on physics experiments conducted.', '2020-01-01', 1, 1, 3, '2020-01-02', 3);

-- Sample data for Enrollment table
INSERT INTO [Enrollment] ([userID], [classID])
VALUES 
    (1, 1),
    (2, 2),
    (3, 3);

-- Sample data for Teaching table
INSERT INTO [Teaching] ([userID], [classID])
VALUES 
    (1, 1),
    (2, 2);

-- Sample data for Managing table
INSERT INTO [Managing] ([userID], [classID])
VALUES 
    (1, 1),
    (3, 3);

-- Sample data for CourseInClass table
INSERT INTO [CourseInClass] ([courseID], [classID])
VALUES 
    (1, 1),
    (2, 2),
    (3, 3);
